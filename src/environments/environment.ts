export const url = {
  gateway : 'http://localhost:9102'
};

export const serviceUrls = {
  project: {
    baseUrl : url.gateway + '/MICROSERVICE-PROJECT'
  },
  media: {
    baseUrl: url.gateway + 'MICROSERVICE-MEDIA'
  }
};


// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  envName: 'local',
  keycloak: {
    // Url of the Identity Provider
    issuer: 'https://www.auth.myproject.ovh/auth/realms/portfolio',

    // URL of the SPA to redirect the user to after login
    // redirectUri: 'http://localhost:4200/',
    redirectUri: window.location.origin,

    // The SPA's id.
    // The SPA is registerd with this id at the auth-server
    clientId: 'portfolio-client',
    dummyClientSecret: '01e7163d-c98a-4335-9936-41152f13fed8',
    responseType: 'code',
    // set the scope for the permissions the client should request
    // The first three are defined by OIDC.
    scope: 'openid profile email',
    // Remove the requirement of using Https to simplify the demo
    // THIS SHOULD NOT BE USED IN PRODUCTION
    // USE A CERTIFICATE FOR YOUR IDP
    // IN PRODUCTION
    requireHttps: false,
    // at_hash is not present in JWT token
    showDebugInformation: true,
    disableAtHashCheck: true
  },
  endpointUrl: {
    project: {
      getAllProject: serviceUrls.project.baseUrl + '/project/All-project',
      getProjectById: serviceUrls.project.baseUrl + '/project/project-by-id',
    }
  }
};
