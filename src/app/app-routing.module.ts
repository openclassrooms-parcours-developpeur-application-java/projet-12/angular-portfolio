import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProjectListComponent} from './project-list/project-list.component';
import {ProjectDetailComponent} from './project-list/project-detail/project-detail.component';
import {AccueilComponent} from './accueil/accueil.component';
import {ProofOfConceptListComponent} from './proof-of-concept-list/proof-of-concept-list.component';
import {ProofOfConceptDetailComponent} from './proof-of-concept-list/proof-of-concept-detail/proof-of-concept-detail.component';
import {UnauthorizedComponent} from './unauthorized/unauthorized.component';
import {CustomGuard} from './custom.guard';
import {ProfilComponent} from './profil/profil.component';

const routes: Routes = [
  { path: 'projectList', component: ProjectListComponent},
  { path: 'projectList/projectDetail', component: ProjectDetailComponent },
  { path: 'accueil/projectDetail', component: ProjectDetailComponent },
  { path: 'proofOfConceptList', component: ProofOfConceptListComponent },
  { path: 'proofOfConceptList/proofOfConceptDetail', component: ProofOfConceptDetailComponent },
  { path: 'accueil/proofOfConceptDetail', component: ProofOfConceptDetailComponent },
  { path: 'accueil', component: AccueilComponent },
  { path: 'profil', component: ProfilComponent},
  { path: 'unauthorized', component: UnauthorizedComponent },
  // { path: '', redirectTo: '/accueil', pathMatch: 'full' },
  { path: '**', redirectTo: '/accueil' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
