import { Component, OnInit } from '@angular/core';
import {ProfilService} from '../service/profil.service';
import {Profil} from '../model/profil';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss'],
  providers: [DatePipe],
})
export class ProfilComponent implements OnInit {

  datePipeNew: Date = new Date();
  age: number;
  profil: Profil;


  constructor(private profilService: ProfilService, public datePipe: DatePipe) { }

  ngOnInit(): void {
    this.onFetchProfil();
  }

  onFetchProfil() {
    this.profilService.getProfilFromServer().subscribe(
      (httpResponse) => {
        this.profil = httpResponse;
      this.age = Number(this.datePipe.transform(this.datePipeNew, 'yyyy')) - Number(this.datePipe.transform(this.profil.dateOfBirth, 'yyyy'));
      },
      (error) => {
        console.log('Error : ' + error);
      }
    );
  }

}
