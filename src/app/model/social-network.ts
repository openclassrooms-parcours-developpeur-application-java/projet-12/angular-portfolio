export class SocialNetwork {

  id: number;
  name: string;
  url: string;
  profilId: number;

  constructor(id: number, name: string, url: string, profilId: number) {
    this.id = id;
    this.name = name;
    this.url = url;
    this.profilId = profilId;
  }
}
