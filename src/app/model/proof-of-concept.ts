import {Article} from './article';
import {Image} from './image';
import {Resource} from './resource';

export class ProofOfConcept {

  id: number;
  name: string;
  publication: string;
  summarize: string;
  imageDTOList: Image[];
  articleDTOList: Article[];
  resourceDTOList: Resource[];

  constructor(id: number, name: string, publication: string, summarize: string, imageDTOList: Image[], articleDTOList: Article[], resourceDTOList: Resource[]) {
    this.id = id;
    this.name = name;
    this.publication = publication;
    this.summarize = summarize;
    this.imageDTOList = imageDTOList;
    this.articleDTOList = articleDTOList;
    this.resourceDTOList = resourceDTOList;
  }
}
