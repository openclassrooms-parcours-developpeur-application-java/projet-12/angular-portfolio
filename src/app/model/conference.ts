import {Image} from './image';
import {Video} from './video';

export class Conference {

  id: number;
  name: string;
  summarize: string;
  profilId: number;
  imageDTOList: Image[];
  videoDTOList: Video[];

  constructor(id: number, name: string, summarize: string, profilId: number, imageDTOList: Image[], videoDTOList: Video[]) {
    this.id = id;
    this.name = name;
    this.summarize = summarize;
    this.profilId = profilId;
    this.imageDTOList = imageDTOList;
    this.videoDTOList = videoDTOList;
  }
}
