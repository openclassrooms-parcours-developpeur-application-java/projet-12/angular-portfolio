import { Component, OnInit } from '@angular/core';
import {ProfilService} from '../service/profil.service';
import {Profil} from '../model/profil';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  profil: Profil;

  constructor(private profilService: ProfilService) { }

  ngOnInit(): void {
    this.onFetchProfil();
  }

  onFetchProfil() {
    this.profilService.getProfilFromServer().subscribe(
      (httpResponse) => {
        this.profil = httpResponse;
      },
      (error) => {
        console.log('Error : ' + error);
      }
    );
  }
}
