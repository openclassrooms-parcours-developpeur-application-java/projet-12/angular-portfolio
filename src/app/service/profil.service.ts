import {Injectable} from '@angular/core';
import {Profil} from '../model/profil';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class ProfilService {

  constructor(private httpClient: HttpClient) {}

  getProfilFromServer(): Observable<Profil> {
    return  this.httpClient
      .get<Profil>('http://localhost:9102/MICROSERVICE-PROFIL/profil/profil/get-full-profil')
      .pipe(map(httpResponse => httpResponse));
  }

}
