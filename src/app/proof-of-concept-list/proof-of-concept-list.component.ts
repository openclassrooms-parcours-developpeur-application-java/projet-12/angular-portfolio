import { Component, OnInit } from '@angular/core';
import {ProofOfConceptService} from '../service/proofOfConcept.service';
import {ProofOfConcept} from '../model/proof-of-concept';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-proof-of-concept-list',
  templateUrl: './proof-of-concept-list.component.html',
  styleUrls: ['./proof-of-concept-list.component.scss'],
  providers: [DatePipe]
})
export class ProofOfConceptListComponent implements OnInit {

  proofOfConceptList: ProofOfConcept[];

  constructor(private proofOfConceptService: ProofOfConceptService, public datePipe: DatePipe) { }

  ngOnInit(): void {
    this.onFetchProofOfConceptList();
  }

  onClickProofOfConceptDetail(proofOfConcept: ProofOfConcept) {
    this.proofOfConceptService.setProofOfConcept(proofOfConcept);
  }

  onFetchProofOfConceptList() {
    this.proofOfConceptService.getAllProofOfConceptFromServer().subscribe(
      (httpResponse) => {
        this.proofOfConceptList = httpResponse;
      },
      (error) => {
        console.log('Error : ' + error);
      }
    );
  }

}
