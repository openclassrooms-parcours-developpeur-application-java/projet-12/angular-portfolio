import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProofOfConceptDetailComponent } from './proof-of-concept-detail.component';

describe('ProofOfConceptDetailComponent', () => {
  let component: ProofOfConceptDetailComponent;
  let fixture: ComponentFixture<ProofOfConceptDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProofOfConceptDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProofOfConceptDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
