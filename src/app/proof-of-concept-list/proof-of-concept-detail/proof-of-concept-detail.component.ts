import { Component, OnInit } from '@angular/core';
import {ProofOfConceptService} from '../../service/proofOfConcept.service';
import {ProofOfConcept} from '../../model/proof-of-concept';
import {Article} from '../../model/article';

@Component({
  selector: 'app-proof-of-concept-detail',
  templateUrl: './proof-of-concept-detail.component.html',
  styleUrls: ['./proof-of-concept-detail.component.scss']
})
export class ProofOfConceptDetailComponent implements OnInit {

  proofOfConcept: ProofOfConcept;

  constructor(private proofOfConceptService: ProofOfConceptService) { }

  ngOnInit(): void {
    this.proofOfConcept = this.proofOfConceptService.getProofOfConcept();
    this.onFetchAllArticleByProofOfConcept(this.proofOfConcept.id);
  }

  onFetchAllArticleByProofOfConcept(proofOfConceptId: number): void {
    this.proofOfConceptService.getAllArticleByProofOfConceptFromServer(proofOfConceptId).subscribe(
      (httpResponse: Article[]) => {
        this.proofOfConcept.articleDTOList = httpResponse;
      },
      (error) => {
        console.log('Error : ' + error);
      }
    );
  }
}
